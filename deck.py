from random import randint, seed
from card import card

class deck :
	""" Class for deck of 52 playing cards """

	def __init__(self) :
		self.numCards = 52
		self.numTypes = 4
		self.numCardsOfEachType = 13
		self.drawnCards = {}
		self.orderOfCards = []
		self.cards = []
		for t in range(1, 1 + self.numTypes) :
			for c in range(1, 1 + self.numCardsOfEachType) :
				self.cards.append(card(t,c))

		for i in range(0, self.numCards) :
			self.drawnCards[i] = 0

		seed()

	def getCards(self, n) :
		cards = []
		for i in range(0, n):
			while True :
				cardIndex = randint(0,51)
				if self.drawnCards[cardIndex] == 0 :
					self.drawnCards[cardIndex] = 1
					self.orderOfCards.append(cardIndex)
					cards.append(self.cards[cardIndex])
					break
		return cards

	def printRemainingDeck(self) :
		for i in range(0, self.numCards):
			if self.drawnCards[i] == 1:
				continue;
			self.cards[i].printCard()

	def printDrawnDeck(self) :
		# prints card in order of dealing
		for i in range(0, len(self.orderOfCards)):
			self.cards[self.orderOfCards[i]].printCard()

	def printDeck(self) :
		for i in range(0, self.numCards):
			self.cards[i].printCard()

# Code to test deck
"""
d = deck()
c = d.getCards(5)
for i in range(0, len(c)):
	c[i].printCard()
print ""
d.printDrawnDeck()
print ""
#d.printRemainingDeck()
"""

		

