from poker import pokerModel

class pokerView :
	def __init__(self, playerMoney, numPlayer = 2, smallBlind = 5, indexWithTurn = 0) :
		self.pm = pokerModel(playerMoney, numPlayer, smallBlind)
		self.indexWithTurn = indexWithTurn

	def getPokerModel(self) :
		return self.pm

	def proceedTurn(self) :
		pm = self.getPokerModel()
		if pm == None :
			return False

		if(pm.getNumberOfActivePlayers() <= 1):
			return False
		while True :
			if(self.indexWithTurn == pm.getNumberOfPlayers() - 1) :
				self.indexWithTurn = 0
			else :
				self.indexWithTurn += 1
			if pm.getNthPlayer(self.indexWithTurn).getPlaying() :
				break
		return True

	def performAction(self, player) :
		pm = self.getPokerModel()
		if pm == None :
			return False

		action = player.getAction(pm.getRaiseAmount())
		if len(action) <= 0 :
			return False
		if action[0] == 1 :
			if len(action) != 2 :
				return False
			return pm.raiseAction(player, action[1])
		elif action[0] == 2 :
			return pm.callAction(player)
		elif action[0] == 3 :
			return pm.foldAction(player)
		else :
			return False

	def performPlayerActions(self) :
		pm = self.getPokerModel()
		if pm == None :
			return False

		if(pm.getNumberOfActivePlayers() <= 1) :
			return False
		color = []
		for i in range(0, pm.getNumberOfPlayers()) :
			color.append(False)
		while pm.getNumberOfActivePlayers() > 1 :
			if color[self.indexWithTurn] and pm.getNthPlayer(self.indexWithTurn).getBet() == pm.getRaiseAmount() :
				break
			color[self.indexWithTurn] = True
			# Check if player is "All In"
			if pm.getNthPlayer(self.indexWithTurn).getAmount() > 0 :
				isSuccess = self.performAction(pm.getNthPlayer(self.indexWithTurn))
				if not(isSuccess) :
					return False
			else :
				print "All in\n"
			self.proceedTurn()
		self.displayTableDistribution()
		pm.collectIntoPot()
		return True

	def displayTableDistribution(self, showPlayerCards = False) :
		pm = self.getPokerModel()
		if pm == None :
			return

		print "============= Current Configuration of table =============\n"
		print "Cards on table : "
		for c in pm.getTableCards() :
			c.printCard()
		print ""

		print "Money in pot : " + str(pm.getPotAmount()) + "\n"

		for i in range(0, pm.getNumberOfPlayers()) :
			print "Player : " + str(pm.getNthPlayer(i).getID() + 1) + "\n"
			if not(pm.getNthPlayer(i).getPlaying()) :
				print "Not Playing\n"
				print "Bet : " + str(pm.getNthPlayer(i).getBet()) + "\n"
				print "Balance Amount : " + str(pm.getNthPlayer(i).getAmount()) + "\n"
				continue
			if showPlayerCards :
				print "Hand cards : "
				for c in pm.getNthPlayer(i).getHand() :
					c.printCard()
				print ""
			print "Bet : " + str(pm.getNthPlayer(i).getBet()) + "\n"
			print "Balance Amount : " + str(pm.getNthPlayer(i).getAmount()) + "\n"

		print "========================== END ==========================\n"

	def getStartTurnIndex(self, index) :
		pm = self.getPokerModel()
		if pm == None :
			return False

		if pm.getNthPlayer(index).getPlaying() :
			return index
		else :
			turn = index
			while True :
				if turn == pm.getNumberOfPlayers() - 1 :
					turn = 0
				else :
					turn += 1
				if pm.getNthPlayer(turn).getPlaying() :
					return turn


	def play(self, startTurnIndex) :
		pm = self.getPokerModel()
		if pm == None :
			return False

		if not(pm.getNthPlayer(startTurnIndex).getPlaying()) :
			return

		self.displayTableDistribution()

		# 1. Put small and big blind in pot
		print "Begin Round"
		pm.resetPot()
		self.indexWithTurn = self.getStartTurnIndex(startTurnIndex)
		pm.resetRaise()
		pm.raiseAction(pm.getNthPlayer(self.indexWithTurn), pm.getSmallBlind())
		self.proceedTurn()
		pm.raiseAction(pm.getNthPlayer(self.indexWithTurn), 2*(pm.getSmallBlind()))
		self.proceedTurn()
		self.displayTableDistribution()

		# 2. Deal cards to all players
		pm.dealCards()
		print pm.getGameState()
		self.performPlayerActions()
		if pm.getNumberOfActivePlayers() == 1 :
			winner = pm.endRound()
			return winner

		# 3. Deal flop
		self.indexWithTurn = self.getStartTurnIndex(startTurnIndex)
		pm.resetRaise()
		pm.showFlop()
		print pm.getGameState()
		self.performPlayerActions()
		if pm.getNumberOfActivePlayers() == 1 :
			winner = pm.endRound()
			return winner

		# 4. Deal Turn Card
		self.indexWithTurn = self.getStartTurnIndex(startTurnIndex)
		pm.resetRaise()
		pm.showTurnCard()
		print pm.getGameState()
		self.performPlayerActions()
		if pm.getNumberOfActivePlayers() == 1 :
			winner = pm.endRound()
			return winner

		# 5. Deal River Card
		self.indexWithTurn = self.getStartTurnIndex(startTurnIndex)
		pm.resetRaise()
		pm.showRiverCard()
		print pm.getGameState()
		self.performPlayerActions()
		
		# 6. Show Player Cards
		winner = pm.endRound()
		print pm.getGameState()

		self.displayTableDistribution()

		return winner



# Code to test poker class

p = pokerView([1000,1000,1000,1000], 4)
winner = p.play(0)

for i in range(0, p.getPokerModel().getNumberOfPlayers()) :
	print "Player : " + str(i + 1)
	for c in p.getPokerModel().getNthPlayer(i).getHand() :
		c.printCard()
	print ""

print ""
for i in range(0, len(winner)) :
	print "Player : " + str(winner[i][0] + 1)
	print p.getPokerModel().getHandRankStr(winner[i][1][0])
	for c in winner[i][1][1] :
		c.printCard()
print ""

for i in range(0, p.getPokerModel().getNumberOfPlayers()) :
	print p.getPokerModel().getNthPlayer(i).getAmount()

# 1. Add support for multiple pots
