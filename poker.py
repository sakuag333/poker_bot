from deck import deck
from pokerPlayer import pokerPlayer
import pokerUtils as pu

class pokerModel :
	""" poker game """

	def __init__(self, playerMoney, numPlayer = 2, smallBlind = 5) :
		self.deck = deck()
		self.state = 0
		self.stateName = ["", "Deal Cards", "Show Flop", "Show Turn Card", "Show River Card", "Show Player Cards"]
		self.playerAction = ["", "Raise", "Call", "Fold"]
		self.handRanks = ["", "Straight Flush", "Four of a kind", "Full house", "Flush", "Straight", "Three of a kind", "Two pair", "One pair", "High card", "Muck"]
		self.pot = 0		# Add support for multiple pots
		self.raiseAmt = 0
		self.numPlayer = numPlayer
		self.smallBlind = smallBlind
		self.players = []
		self.tableCards = []
		self.numActivePlayer = numPlayer
		for i in range(0, self.numPlayer) :
			self.players.append(pokerPlayer(playerMoney[i], i))

	def getPotAmount(self) :
		return self.pot

	def getRaiseAmount(self) :
		return self.raiseAmt

	def getNumberOfPlayers(self) :
		return self.numPlayer

	def setRaiseAmount(self, amt) :
		self.raiseAmt = amt

	def getNumberOfActivePlayers(self) :
		return self.numActivePlayer

	def getNthPlayer(self, n) :
		if n >= self.numPlayer :
			return None
		return self.players[n]

	def getTableCards(self) :
		return self.tableCards

	def getSmallBlind(self) :
		return self.smallBlind

	def getHandRankStr(self, rank) :
		return self.handRanks[rank]

	def resetPot(self) :
		self.pot = 0

	def resetRaise(self) :
		self.raiseAmt = 0

	def dealCards(self) :
		self.state = 1
		for i in range(0, self.numPlayer) :
			cards = self.deck.getCards(2)
			for j in range(0, len(cards)) :
				self.players[i].dealCard(cards[j])

	def showFlop(self) :
		self.state = 2
		flop = self.deck.getCards(3)
		for i in range(0, len(flop)) :
			self.tableCards.append(flop[i])

	def showTurnCard(self) :
		self.state = 3
		turnCard = self.deck.getCards(1)
		for i in range(0, len(turnCard)) :
			self.tableCards.append(turnCard[i])

	def showRiverCard(self) :
		self.state = 4
		riverCard = self.deck.getCards(1)
		for i in range(0, len(riverCard)) :
			self.tableCards.append(riverCard[i])

	def endRound(self) :
		self.state = 5
		winner = self.getWinner()

		# Distribute money to winner(s)
		winAmt = self.pot/len(winner)
		for w in winner :
			self.players[w[0]].incr(winAmt)

		self.pot = 0
		return winner

	def raiseAction(self, player, amount) :
		if not(player.getPlaying()) :
			return False
		# First call , then raise with said amount
		if amount < self.raiseAmt :
			return False
		if player.getAmount() < amount :
			return False
		player.decr(amount - player.getBet())
		#self.pot += amount
		self.raiseAmt = amount
		player.setBet(amount)
		return True

	def callAction(self, player) :
		if not(player.getPlaying()) :
			return False
		toRaise = self.raiseAmt - player.getBet()
		if player.getAmount() < toRaise :
			#self.pot += player.getAmount()
			player.setBet(player.getBet() + player.getAmount())
		else :
			#self.pot += raiseAmt
			player.setBet(self.raiseAmt)
		player.decr(toRaise)
		return True

	def foldAction(self, player) :
		if not(player.getPlaying()) :
			return False
		player.setPlaying(False)
		self.numActivePlayer -= 1
		return True

	def getGameState(self) :
		return self.stateName[self.state]

	def collectIntoPot(self) :
		for i in range(0, self.numPlayer) :
			self.pot += self.players[i].getBet()
			self.players[i].setBet(0)

	def getWinner(self) :
		# 1. Straight Flush
		# 2. Four of a kind
		# 3. Full House
		# 4. Flush
		# 5. Straight
		# 6. Three of a kind
		# 7. Two pair
		# 8. One pair
		# 9. High card
		playerHandRank = {}
		highestRank = 100
		for i in range(0, self.numPlayer) :
			if self.players[i].getPlaying() :
				cards = []
				for c in self.players[i].getHand() :
					cards.append(c)
				for c in self.tableCards :
					cards.append(c)
				playerHandRank[i] = pu.getHandRank(cards)
				if(playerHandRank[i][0] < highestRank) :
					highestRank = playerHandRank[i][0]
			else :
				playerHandRank[i] = (-1, -1)

		winnerCandidate = []
		for i in range(0, self.numPlayer) :
			if self.players[i].getPlaying() :
				if playerHandRank[i][0] == highestRank :
					winnerCandidate.append((i, playerHandRank[i])) # player id , (hand rank , cards)

		candidateWinCnt = []
		for i in range(0, len(winnerCandidate)) :
			candidateWinCnt.append(0)
		for i in range(0, len(winnerCandidate)) :
			for j in range(i + 1, len(winnerCandidate)) :
				winner = pu.compareCards(winnerCandidate[i][1], winnerCandidate[j][1])
				if (winner == 1) :
					candidateWinCnt[i] += 1
				elif (winner == 2) :
					candidateWinCnt[j] += 1

		maxCnt = -1
		for i in range(0, len(winnerCandidate)) :
			if candidateWinCnt[i] > maxCnt :
				maxCnt = candidateWinCnt[i]

		winners = []
		for i in range(0, len(winnerCandidate)) :
			if candidateWinCnt[i] == maxCnt :
				winners.append(winnerCandidate[i])

		return winners
