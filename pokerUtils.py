def getFlush(cards) :
	cards = sorted(cards, reverse = True)
	cardTypeFreq = {}
	for i in range(1,5) :
		cardTypeFreq[i] = 0
	for i in range(0, len(cards)) :
		cardTypeFreq[cards[i].getType()] += 1

	flushType = -1
	for i in range(1,5) :
		if cardTypeFreq[i] >= 5 :
			flushType = i
			break
	flush = []
	if flushType == -1 :
		return flush
	for i in range(0, len(cards)) :
		if cards[i].getType() == flushType :
			if cards[i].getValue() == 1 :
				flush.append(cards[i])

	for i in range(0, len(cards)) :
		if len(flush) == 5 :
			break
		if cards[i].getType() == flushType :
			if cards[i].getValue() != 1 :
				flush.append(cards[i])

	return flush

def getStraight(cards) :
	repCards = sorted(cards, reverse = True)

	#remove repition
	cards = []
	cards.append(repCards[0])
	for i in range(1, len(repCards)) :
		if repCards[i].getValue() == repCards[i-1].getValue() :
			continue
		cards.append(repCards[i])

	cnt = 1
	start = 0
	for i in range(1, len(cards)) :
		if cnt == 5 :
			break
		if cards[i-1].getValue() - cards[i].getValue() == 1 :
			cnt += 1
		else :
			cnt = 1
			start = i

	straight = []
	if cnt < 5 :
		# check for Ace 2 3 4 5
		if cards[0].getValue() != 14 :
			return []
		cnt = 2
		for i in range(len(cards) - 1, 0, -1) :
			if cnt == 6 :     # checking for 6 means, cnt incremented till 5, hence cards checked till 5, as cnt is compared for card value
				break
			if cards[i].getValue() == cnt :
				cnt += 1
			else :
				break
		if cnt < 5 :
			return []
		for i in range(len(cards) - 4, len(cards)) :
			straight.append(cards[i])
		straight.append(cards[0])
		return straight
		
	for i in range(start , start + 5) :
		straight.append(cards[i])
	return straight

def getNOfAKind(cards, n) :
	cards = sorted(cards, reverse = True)
	cnt = 1
	start = 0
	for i in range(1, len(cards)) :
		if cnt == n :
			break
		if cards[i-1].getValue() == cards[i].getValue() :
			cnt += 1
		else :
			cnt = 1
			start = i

	NOfKind = []
	if cnt < n :
		return NOfKind
	for i in range(start , start + n) :
		NOfKind.append(cards[i])

	remainingCards = []
	for c1 in cards :
		isPresent = False
		for c2 in NOfKind :
			if c1 == c2 :
				isPresent = True
				break
		if not(isPresent) :
			remainingCards.append(c1)

	cnt = n
	for c in remainingCards :
		if cnt == 5 :
			break
		NOfKind.append(c)
		cnt += 1

	return NOfKind

def getStraightFlush(cards) :
	cards = sorted(cards, reverse = True)
	cardTypeFreq = {}
	for i in range(1,5) :
		cardTypeFreq[i] = 0
	for i in range(0, len(cards)) :
		cardTypeFreq[cards[i].getType()] += 1

	flushType = -1
	for i in range(1,5) :
		if cardTypeFreq[i] >= 5 :
			flushType = i
			break
	flush = []
	if flushType == -1 :
		return flush
	for i in range(0, len(cards)) :
		if cards[i].getType() == flushType :
				flush.append(cards[i])

	return getStraight(flush)

def getTwoPair(cards) :
	twoPairCards = []
	pairCards = getNOfAKind(cards, 2)
	if len(pairCards) == 0 :
		return []

	for i in range(0, 2) :
		twoPairCards.append(pairCards[i])

	remainingCards = []
	for c1 in cards :	
		isPresent = False
		for c2 in twoPairCards:
			if c1 == c2 :
				isPresent = True
				break
		if not(isPresent) :
			remainingCards.append(c1)

	anothePairCards = getNOfAKind(remainingCards, 2)
	if len(anothePairCards) == 0 :
		return []

	for i in range(0, 2) :
		twoPairCards.append(anothePairCards[i])

	remainingCards2 = []
	for c1 in remainingCards :
		isPresent = False
		for c2 in twoPairCards :
			if c1 == c2 :
				isPresent = True
				break
		if not(isPresent) :
			remainingCards2.append(c1)

	remainingCards2 = sorted(remainingCards2, reverse = True)
	twoPairCards.append(remainingCards2[0])

	return twoPairCards

def getFullHouse(cards) :
	threeOFKind = getNOfAKind(cards, 3)
	if len(threeOFKind) == 0 :
		return []

	fullHouse = []
	for i in range(0, 3) :
		fullHouse.append(threeOFKind[i])

	remainingCards = []
	for c1 in cards :
		isPresent = False
		for c2 in threeOFKind :
			if c1 == c2 :
				isPresent = True
				break
		if not(isPresent) :
			remainingCards.append(c1)

	twoOfKind = getNOfAKind(remainingCards, 2)
	if len(twoOfKind) == 0 :
		return []

	for i in range(0, 2) :
		fullHouse.append(twoOfKind[i])

	return fullHouse

def getHighCard(cards) :
	cards = sorted(cards, reverse = True)
	highCard = []
	for i in range(0, 5) :
		highCard.append(cards[i])
	return highCard

def compareCards(tuple1, tuple2) : # tuple -> (hand rank, cards)
	rank = tuple1[0]
	cards1 = tuple1[1]
	cards2 = tuple2[1]
	if rank == 1 :
		return compStraightFlush(cards1, cards2)
	elif rank == 2 or rank == 6  or rank == 8:
		return compNOfAkind(cards1, cards2)
	elif rank == 3 :
		return compFullHouse(cards1, cards2)
	elif rank == 4 :
		return compFlush(cards1, cards2)
	elif rank == 5 :
		return compStraight(cards1, cards2)
	elif rank == 7 :
		return compTwoPair(cards1, cards2)
	elif rank == 9 :
		return compHighCard(cards1, cards2)
	return 0

def compFlush(cards1, cards2) :
	for i in range(0, 5) :
		if cards1[i].getValue() > cards2[i].getValue() :
			return 1
		elif cards2[i].getValue() > cards1[i].getValue() :
			return 2
	return 0

def compTwoPair(cards1, cards2) :
	for i in range(0, 5) :
		if cards1[i].getValue() > cards2[i].getValue() :
			return 1
		elif cards2[i].getValue() > cards1[i].getValue() :
			return 2
	return 0

def compStraight(cards1, cards2) :
	if cards1[0].getValue() < cards2[0].getValue() :
		return 1
	elif cards2[0].getValue() < cards1[0].getValue() :
		return 2
	return 0

def compStraightFlush(cards1, cards2) :
	return straight(cards1, cards2)

def compNOfAkind(cards1, cards2) :
	for i in range(0, 5) :
		if cards1[i].getValue() > cards2[i].getValue() :
			return 1
		elif cards2[i].getValue() > cards1[i].getValue() :
			return 2
	return 0

def compFullHouse(cards1, cards2) :
	for i in range(0, 5) :
		if cards1[i].getValue() > cards2[i].getValue() :
			return 1
		elif cards2[i].getValue() > cards1[i].getValue() :
			return 2
	return 0

def compHighCard(cards1, cards2) :
	cards1 = sorted(cards1, reverse = True)
	cards2 = sorted(cards2, reverse = True)
	for i in range(0, 5) :
		if cards1[i].getValue() > cards2[i].getValue() :
			return 1
		elif cards2[i].getValue() > cards1[i].getValue() :
			return 2
	return 0

def getHandRank(cards) :
	if len(cards) < 5 :
		return (10, cards)

	rankCards = []

	# 1. Straight Flush
	rankCards = getStraightFlush(cards)
	if len(rankCards) > 0 :
		return (1, rankCards)

	# 2. Four of a kind
	rankCards = getNOfAKind(cards, 4)
	if len(rankCards) > 0 :
		return (4, rankCards)

	# 3. Full house
	rankCards = getFullHouse(cards)
	if len(rankCards) > 0 :
		return (3, rankCards)

	# 4. Flush
	rankCards = getFlush(cards)
	if len(rankCards) > 0 :
		return (4, rankCards)

	# 5. Straight
	rankCards = getStraight(cards)
	if len(rankCards) > 0 :
		return (5, rankCards)

	# 6. Three of a Kind
	rankCards = getNOfAKind(cards, 3)
	if len(rankCards) > 0 :
		return (6, rankCards)

	# 7. Two pair
	rankCards = getTwoPair(cards)
	if len(rankCards) > 0 :
		return (7, rankCards)

	# 8. One Pair
	rankCards = getNOfAKind(cards, 2)
	if len(rankCards) > 0 :
		return (8, rankCards)

	# 9. High Card
	rankCards = getHighCard(cards)
	return (9, rankCards)
