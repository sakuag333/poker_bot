from player import player
from random import randint, seed

class pokerPlayer (player) :
	""" player who knows how to playe poker """

	def __init__(self, money, pokerID) :
		player.__init__(self, money)
		self.id = pokerID
		self.actionsName = ["", "Raise", "Call", "Fold"]
		self.bet = 0
		seed()

	def getAction(self, currRaise) :
		# 1 - Raise , 2 - Call / Check , 3 - Fold
		action = randint(1,3)
		raiseMoney = randint(1 + currRaise, currRaise + 10)	# Valid only if action is raise, otherwise ignored
		print "Action of Player : " + str(self.getID() + 1)
		print "Action : ", self.actionsName[action], " ", raiseMoney
		print ""
		return (action, raiseMoney)
		"""
		print "Action of Player : " + str(self.getID() + 1)
		return (2,)
		"""

	def getID(self) :
		return self.id

	def getBet(self) :
		return self.bet

	def setBet(self, bet) :
		self.bet = bet