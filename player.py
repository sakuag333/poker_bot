class player :
	""" Class to represent a poker player """

	def __init__(self, money) :
		self.money = money
		self.hand = []
		self.playing = True

	def getAmount(self) :
		return self.money

	def getHand(self) :
		return self.hand

	def dealCard(self, card) :
		self.hand.append(card)

	def incr(self, k) :
		self.money += k

	def decr(self, k) :
		self.money -= k
		if self.money < 0 :
			self.money = 0

	def setPlaying(self, isPlaying) :
		self.playing = isPlaying

	def getPlaying(self) :
		return self.playing
