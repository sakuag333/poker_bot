class card :
	""" Class for playing cards """

	def __init__(self, cardType, cardVal) :
		self.type = cardType
		if cardVal == 1 :
			cardVal = 14
		self.num = cardVal
		self.TypesOfCard = ["", "Hearts", "Diamonds", "Spades", "Clubs"]
		self.NumOnCards = ["", "Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King", "Ace"]

	def get(self) :
		if self.type > len(self.TypesOfCard) or self.num > len(self.NumOnCards) or self.type <= 0 or self.type <= 0 :
			return ("Invalid Card", "Invalid Card")
		return (self.TypesOfCard[self.type], self.NumOnCards[self.num])

	def printCard(self) :
		str = self.NumOnCards[self.num] + " of " + self.TypesOfCard[self.type]
		print str

	def getType(self) :
		return self.type

	def getValue(self) :
		return self.num

	def __gt__(card1, card2) :
		return card1.getValue() > card2.getValue()

	def __lt__(card1, card2) :
		return card1.getValue() < card2.getValue()
